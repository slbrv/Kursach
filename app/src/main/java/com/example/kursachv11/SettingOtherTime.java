package com.example.kursachv11;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SettingOtherTime extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_other_time);
    }

    boolean chisloW = false;
    boolean chisloG = false;
    EditText timeWakeUp, timeGo, howManyWake, howManyOut, intervalAlarm;
    int HowWake = 2, HowOut = 1;

    public void learn (View v)
    {
        timeWakeUp = (EditText) findViewById(R.id.timeToWakeUp);
        timeGo = (EditText) findViewById(R.id.timeToGoInUniversity);
        howManyWake = (EditText) findViewById(R.id.howManyToWakeUp);
        howManyOut = (EditText) findViewById(R.id.howManyToGoOut);
        intervalAlarm = (EditText) findViewById(R.id.intervalAlarm);

        int longTextWake = timeWakeUp.getText().toString().length();
        int longTextG = timeGo.getText().toString().length();

        int interval = 5;

        if(howManyWake.getText().toString().length() != 0)
        {
            HowMany(Integer.parseInt(howManyWake.getText().toString()), -100, -210, true);
        }

        if(howManyOut.getText().toString().length() != 0)
        {
            HowMany(Integer.parseInt(howManyOut.getText().toString()), -100, -40, false);
        }

        if(intervalAlarm.getText().toString().length() != 0)
        {
            try
            {
                interval = Integer.parseInt(intervalAlarm.getText().toString());
            }
            catch (NumberFormatException e)
            {
                InputNumber(50, 140, "Введите число");
            }
        }

        int hTimeG = 0;
        int mTimeG = 0;
        int timeG = 0;

        if (longTextG != 0)
        {
            try
            {
                timeG = Integer.parseInt(timeGo.getText().toString()); //считываем время на путь

                if ((timeG / 100 < 24) && (timeG % 100 < 60))
                {
                    hTimeG = timeG / 100;
                    mTimeG = timeG % 100;
                    chisloG = true;
                }
                else
                {
                    Toast.makeText(this, "Число не соответствует временным размерностям", Toast.LENGTH_LONG).show();
                }
            }
            catch (NumberFormatException e)
            {
                InputNumber(200, -420, "Введите число");
            }
        }
        else
        {
            InputNumber(200, -420, "Введите число (время на путь)");
        }

        int hTimeWake = 0;
        int mTimeWake = 0;
        int timeWake = 0;

        if (longTextWake != 0)
        {
            try
            {
                timeWake = Integer.parseInt(timeWakeUp.getText().toString()); //считываем время на встать

                if ((timeWake / 100 < 24) && (timeWake % 100 < 60))
                {
                    hTimeWake = timeWake / 100;
                    mTimeWake = timeWake % 100;
                    chisloW = true;
                }
                else
                {
                    Toast.makeText(this, "Число не соответствует временным размерностям", Toast.LENGTH_SHORT).show();
                }
            }
            catch (NumberFormatException e)
            {
                InputNumber(155, -530, "Введите число");
            }
        }
        else
        {
            InputNumber(155, -530, "Введите число (время на проснутся)");
        }

        TimeFirstLesson.howManyWake = HowWake;
        TimeFirstLesson.howManyOut  = HowOut;
        TimeFirstLesson.timeWakeUpH = hTimeWake;
        TimeFirstLesson.timeWakeUpM = mTimeWake;
        TimeFirstLesson.timeGoH = hTimeG;
        TimeFirstLesson.timeGoM = mTimeG;
        TimeFirstLesson.chisloG = chisloG;
        TimeFirstLesson.chisloW = chisloW;
        TimeFirstLesson.timeInterval = interval;

        MgtuBaumanka.howManyWake = HowWake;
        MgtuBaumanka.howManyOut  = HowOut;
        MgtuBaumanka.timeWakeUpH = hTimeWake;
        MgtuBaumanka.timeWakeUpM = mTimeWake;
        MgtuBaumanka.timeGoH = hTimeG;
        MgtuBaumanka.timeGoM = mTimeG;
        MgtuBaumanka.timeInterval = interval;
        MgtuBaumanka.chisloG = chisloG;
        MgtuBaumanka.chisloW = chisloW;
    }

    public void HowMany (int Number, int x, int y, boolean WO)
    {
        try
        {
            if(Number > 0)
            {
                if(Number < 10)
                {
                    if (WO == true)
                    {
                        HowWake = Number;
                        Log.i("LearnTime", HowWake + " Время");
                    }
                    else
                    {
                        HowOut = Number;
                        Log.i("LearnTime", HowOut + " Время");
                    }
                }
                else
                {
                    InputNumber(x, y, "Введите число не более чем 10");
                }
            }
            else
            {
                InputNumber(x, y, "Введите число >= 0");
            }
        }
        catch (NumberFormatException e)
        {
            InputNumber(x, y, "Введите число");
        }
    }

    public void InputNumber(int x, int y, String s)
    {
        Toast toast = Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, x, y);
        toast.show();
    }

    public void onButtonClickTimeFirstLessonWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(SettingOtherTime.this, TimeFirstLesson.class);
        startActivity(intent);
    }

    public void onButtonClickmgtuLessonWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(SettingOtherTime.this, MgtuBaumanka.class);
        startActivity(intent);
    }

    public void onButtonClickMainActivityWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(SettingOtherTime.this, MainActivity.class);
        startActivity(intent);
    }
}