package com.example.kursachv11;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;

public class MgtuBaumanka extends AppCompatActivity
{
    Switch sw1, sw2;
    EditText timeMondayMgtu, timeTuesdayMgtu, timeWednesdayMgtu, timeThursdayMgtu, timeFridayMgtu, timeSaturdayMgtu;
    public static int  timeWakeUpH, timeWakeUpM, timeGoH, timeGoM, howManyWake, howManyOut, timeInterval;
    public static int nowWeek = 2;
    int timeMMgtu, timeTuMgtu, timeWMgtu, timeThMgtu, timeFMgtu, timeSMgtu; //время, которое передается уже именно в будильники
    int longTextMMgtu, longTextTuesMgtu, longTextWenMgtu, longTextThurMgtu, longTextFMgtu, longTextSMgtu;
    static int timeMCH = 0, timeTuesCH = 0, timeWCH = 0, timeThurCH = 0, timeFCH = 0, timeSCH = 0,  timeMZN = 0, timeTuesZN = 0, timeWZN = 0, timeThurZN = 0, timeFZN = 0, timeSZN = 0;
    public static boolean chisloW, chisloG;
    ArrayList<Integer> alarmDays= new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mgtu_baumanka);

        sw1 = (Switch) findViewById(R.id.switch1);
        sw2 = (Switch) findViewById(R.id.switch2);

        sw1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((Switch) v).isChecked();
                if (checked){
                    nowWeek = 0; //числитель
                    Log.i("LearnTime", nowWeek + " Числитель"); //для отладки, высвечивает текст в консоли logcat
                }
                else
                {
                    nowWeek = 2;
                   Log.i("LearnTime", nowWeek + " Нет недели"); //для отладки, высвечивает текст в консоли logcat
                }
            }
        });

        sw2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((Switch) v).isChecked();
                if (checked){
                    nowWeek = 1; //заменатель
                    Log.i("LearnTime", nowWeek + " Знаменатель"); //для отладки, высвечивает текст в консоли logcat
                }
                else
                {
                    nowWeek = 2;
                    Log.i("LearnTime", nowWeek + " Нет недели"); //для отладки, высвечивает текст в консоли logcat
                }
            }
        });
    }

    public void learnTime(View v)
    {
        int i = 0; //для проверки

        Log.i("LearnTime", i + " Заход в learn");

        timeMondayMgtu = (EditText) findViewById(R.id.timeInMondayMgtu);
        timeTuesdayMgtu = (EditText) findViewById(R.id.timeInTuesdayMgtu);
        timeWednesdayMgtu = (EditText) findViewById(R.id.timeInWednesdayMgtu);
        timeThursdayMgtu = (EditText) findViewById(R.id.timeInThursdayMgtu);
        timeFridayMgtu = (EditText) findViewById(R.id.timeInFridayMgtu);
        timeSaturdayMgtu = (EditText) findViewById(R.id.timeInSaturdayMgtu);

        longTextMMgtu = timeMondayMgtu.getText().toString().length();
        longTextTuesMgtu = timeTuesdayMgtu.getText().toString().length();
        longTextWenMgtu = timeWednesdayMgtu.getText().toString().length();
        longTextThurMgtu = timeThursdayMgtu.getText().toString().length();
        longTextFMgtu = timeFridayMgtu.getText().toString().length();
        longTextSMgtu = timeSaturdayMgtu.getText().toString().length();

        if (longTextMMgtu != 0)
        {
            Log.i("LearnTime", i + " Заход в понедельник");
            if (nowWeek == 0) {
                timeMCH = Integer.parseInt(timeMondayMgtu.getText().toString());
                Log.i("LearnTime", timeMCH + " Запоминание числителя");//для отладки, высвечивает текст в консоли logcat
            }
            else if (nowWeek == 1) {
                timeMZN = Integer.parseInt(timeMondayMgtu.getText().toString());
                Log.i("LearnTime", timeMZN + " Запоминание знаменателя");
            }
        }
        if (longTextTuesMgtu != 0) {
            if (nowWeek == 0) {
                timeTuesCH = Integer.parseInt(timeTuesdayMgtu.getText().toString());
            }
            else if (nowWeek == 1) {
                timeTuesZN = Integer.parseInt(timeTuesdayMgtu.getText().toString());
            }
        }
        if (longTextWenMgtu != 0) {
            if (nowWeek == 0) {
                timeWCH = Integer.parseInt(timeWednesdayMgtu.getText().toString());
            }
            else if (nowWeek == 1) {
                timeWZN = Integer.parseInt(timeWednesdayMgtu.getText().toString());
            }
        }
        if (longTextThurMgtu != 0) {
            if (nowWeek == 0) {
                timeThurCH = Integer.parseInt(timeThursdayMgtu.getText().toString());
            }
            else if (nowWeek == 1) {
                timeThurZN = Integer.parseInt(timeThursdayMgtu.getText().toString());
            }
        }
        if (longTextFMgtu != 0) {
            if (nowWeek == 0) {
                timeFCH = Integer.parseInt(timeFridayMgtu.getText().toString());
            }
            else if (nowWeek == 1) {
                timeFZN = Integer.parseInt(timeFridayMgtu.getText().toString());
            }
        }
        if (longTextSMgtu != 0) {
            if (nowWeek == 0) {
                timeSCH = Integer.parseInt(timeSaturdayMgtu.getText().toString());
            }
            else if (nowWeek == 1) {
                timeSZN = Integer.parseInt(timeSaturdayMgtu.getText().toString());
            }
        }
    }

    public void ChooseWeek()
    {
        if (nowWeek == 0) //числитель
        {
            DeleteToChange();
            TimeOnWeek();
            CreateParametersForAlarm();
        }
        else if (nowWeek == 1) //знаменатель
        {
            DeleteToChange();
            TimeOnWeek();
            CreateParametersForAlarm();
        }
    }

    public void DeleteToChange()
    {

    }

    public void TimeOnWeek()
    {
        if (nowWeek == 0) //числитель
        {
            int i = 0;
            //Log.i("LearnTime", i + " Запоминание всего времени на числитель");
            timeMMgtu = timeMCH;  //пофиксить часы, нельзя поставить будильник на полночь
            //Log.i("LearnTime", timeMMgtu + " Запоминание понедельника на числитель");
            timeTuMgtu = timeTuesCH;
            timeWMgtu = timeWCH;
            timeThMgtu = timeThurCH;
            timeFMgtu = timeFCH;
            timeSMgtu = timeSCH;
        }
        else if (nowWeek == 1) //знаменатель
        {
            int i = 0;
            //Log.i("LearnTime", i + " Запоминание всего времени на знаменатель");
            timeMMgtu = timeMZN;
            //Log.i("LearnTime", timeMMgtu + " Запоминание понедельника на знаменатель");
            timeTuMgtu = timeTuesZN;
            timeWMgtu = timeWZN;
            timeThMgtu = timeThurZN;
            timeFMgtu = timeFZN;
            timeSMgtu = timeSZN;
        }
    }

    public void CreateTimeOnClicnk(View v)
    {
        if ((chisloW == true) && (chisloG == true)) //если введено доп время
        {
            int i = 6;
            if(nowWeek == 0) //числитель
            {
                TimeOnWeek();
                CreateParametersForAlarm();
            }
            else if (nowWeek == 1) //знаменатель
            {
                TimeOnWeek();
                CreateParametersForAlarm();
            }
            //Log.i("LearnTime", i + " Создание на числитель");
            //Log.i("LearnTime", i + " Создание на знаменатель");
        }
        else
        {
            Toast.makeText(this, "Не указано доп. время (на подъем и на сборы)", Toast.LENGTH_LONG).show();
        }
    }

    public void CreateParametersForAlarm()
    {
        int i = 0;
        //Log.i("LearnTime", i + " Заход в конкретное создание");
        if ((timeMMgtu != 0) || (timeTuMgtu != 0) || (timeWMgtu != 0) || (timeThMgtu != 0) || (timeFMgtu != 0) || (timeSMgtu != 0))
        {
            if (timeMMgtu != 0)
            {
                int time = timeMMgtu;
                //Log.i("LearnTime", timeMMgtu + " Время на понедельник");
                //Log.i("LearnTime", time + " Время на понедельник");
                ArrayList<Integer> alarmDaysM = new ArrayList<>();
                int dayOfWeek = Calendar.MONDAY;
                int x = -140;
                int y = -490;
                AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval);
            }

            if (timeTuMgtu != 0)
            {
                int time = timeTuMgtu; //пофиксить часы, нельзя поставить будильник на полночь
                int dayOfWeek = Calendar.TUESDAY;
                int x = -140;
                int y = -380;
                AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval);
            }

            if (timeWMgtu != 0)
            {
                int time = timeWMgtu; //пофиксить часы, нельзя поставить будильник на полночь
                int dayOfWeek = Calendar.WEDNESDAY;
                int x = -140;
                int y = -270;
                AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval);
            }

            if (timeThMgtu != 0)
            {
                int time = timeThMgtu; //пофиксить часы, нельзя поставить будильник на полночь
                int dayOfWeek = Calendar.TUESDAY;
                int x = -140;
                int y = -160;
                AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval);
            }

            if (timeFMgtu != 0)
            {
                int time = timeFMgtu; //пофиксить часы, нельзя поставить будильник на полночь
                int dayOfWeek = Calendar.FRIDAY;
                int x = -140;
                int y = -34;
                AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval);
            }

            if (timeSMgtu != 0)
            {
                int time = timeSMgtu; //пофиксить часы, нельзя поставить будильник на полночь
                int dayOfWeek = Calendar.SATURDAY;
                int x = -140;
                int y = 80;
                AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval);
            }

            Toast.makeText(this, "Будильники созданы", Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(this, "Не введено время ни одной пары", Toast.LENGTH_LONG).show();
        }
    }

    public void AlarmCreate(int hTimeG, int mTimeG, int hTimeWake, int mTimeWake, int dayOfWeek, int time, int x, int y, int howWake, int howOut, int interval)
    {
        try
        {
            Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);

            int hTime = time / 100;
            int mTime = time % 100;

            if (hTime < 24 && mTime < 60) //если минуты и часы введены корректно
            {
                hTime -= hTimeG; //вычитаем часы на путь

                if (mTime - mTimeG < 0) //если при вычитании минут на пусть получается минут
                {
                    hTime--; //получается сдвинулся час
                    mTime = 60 + mTime - mTimeG; //и выставляем правильные минуты
                }
                else
                {
                    mTime -= mTimeG;
                }

                int count = 0;

                while (howOut != 0)
                {
                    intent.putExtra(AlarmClock.EXTRA_HOUR, hTime);
                    intent.putExtra(AlarmClock.EXTRA_MINUTES, mTime);
                    alarmDays.add(dayOfWeek);
                    intent.putExtra(AlarmClock.EXTRA_DAYS, alarmDays);
                    startActivity(intent);

                    if (mTime + interval == 60)
                    {
                        hTime++;
                        mTime = 0;
                    }
                    else if (mTime > 60)
                    {
                        mTime -= 60;
                        hTime++;
                    }
                    else
                    {
                        mTime += interval;
                    }

                    count++;
                    howOut--;
                }

                if (count > 0)
                {
                    while (count != 0)
                    {
                        mTime -= interval;
                        count--;
                    }
                }

                hTime -= hTimeWake; //вычитаем время на подъем

                if (mTime - mTimeWake < 0)
                {
                    hTime--;
                    mTime = 60 + mTime - mTimeWake;
                }
                else
                {
                    mTime -= mTimeWake;
                }

                while (howWake != 0)
                {
                    intent.putExtra(AlarmClock.EXTRA_HOUR, hTime);
                    intent.putExtra(AlarmClock.EXTRA_MINUTES, mTime);
                    alarmDays.add(dayOfWeek);
                    intent.putExtra(AlarmClock.EXTRA_DAYS, alarmDays);
                    startActivity(intent);

                    if (mTime + interval == 60)
                    {
                        hTime++;
                        mTime = 0;
                    }
                    else if (mTime > 60)
                    {
                        mTime -= 60;
                        hTime++;
                    }
                    else
                    {
                        mTime += interval;
                    }

                    howWake--;
                }
            }
            else
            {
                Toast.makeText(this, "Число не соответствует временным размерностям", Toast.LENGTH_LONG).show();
            }
        }
        catch (NumberFormatException e)
        {
            Toast toast = Toast.makeText(getApplicationContext(), "Введите число", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, x, y);
            toast.show();
        }
    }

    public void onButtonDeleteAlarm(View v)
    {

    }

    public void onButtonStandardPlanTimeWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(MgtuBaumanka.this, TimeFirstLesson.class);
        startActivity(intent);
    }

    public void onButtonClickSettingOtherTimeWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(MgtuBaumanka.this, SettingOtherTime.class);
        startActivity(intent);
    }

    public void onButtonClickMainActivityWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(MgtuBaumanka.this, MainActivity.class);
        startActivity(intent);
    }
}