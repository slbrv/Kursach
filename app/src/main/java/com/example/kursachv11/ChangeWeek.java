package com.example.kursachv11;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class ChangeWeek extends AppCompatActivity {

    EditText timeSunday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_week);
    }

    public void onButtonClickChangeWeek(View v)
    {
        MgtuBaumanka object = new MgtuBaumanka();

        if (MgtuBaumanka.nowWeek == 1)
        {
            MgtuBaumanka.nowWeek = 0;
            Toast toast = Toast.makeText(getApplicationContext(), "Неделя сменена на числитель", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 460);
            toast.show();
        }
        else if (MgtuBaumanka.nowWeek == 0)
        {
            MgtuBaumanka.nowWeek = 1;
            Toast toast = Toast.makeText(getApplicationContext(), "Неделя сменена на знаменатель", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER, 0, 460);
            toast.show();
        }

        object.ChooseWeek();
    }

    public void CreateTime(View v)
    {
        timeSunday = (EditText) findViewById(R.id.timeInSunday);

        if(timeSunday.getText().toString().length() != 0)
        {
            try
            {
                int time = Integer.parseInt(timeSunday.getText().toString());
                Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);

                int hTime = time / 100;
                int mTime = time % 100;

                if (hTime < 24 && mTime < 60) //если минуты и часы введены корректно
                {

                    intent.putExtra(AlarmClock.EXTRA_HOUR, hTime);
                    intent.putExtra(AlarmClock.EXTRA_MINUTES, mTime);
                    intent.putExtra(AlarmClock.EXTRA_DAYS, Calendar.SUNDAY);
                    intent.putExtra(AlarmClock.EXTRA_MESSAGE, "Поменять неделю");
                    startActivity(intent);
                }
                else
                {
                    Toast toast = Toast.makeText(getApplicationContext(), "Число не соответствует временным размерностям", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 460);
                    toast.show();
                }
            }
            catch (NumberFormatException e)
            {
                Toast toast = Toast.makeText(getApplicationContext(), "Введите число", Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 460);
                toast.show();
            }
        }
    }

    public void onButtonClickmgtuLessonWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(ChangeWeek.this, MgtuBaumanka.class);
        startActivity(intent);
    }

    public void onButtonClickMainActivityWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(ChangeWeek.this, MainActivity.class);
        startActivity(intent);
    }
}