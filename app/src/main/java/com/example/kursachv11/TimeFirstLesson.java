package com.example.kursachv11;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

public class TimeFirstLesson extends AppCompatActivity
{
    EditText timeMondey, timeTuesday, timeWednesday, timeThursday, timeFriday, timeSaturday;
    public static int  timeWakeUpH, timeWakeUpM, timeGoH, timeGoM, howManyWake, howManyOut, timeInterval;
    public static boolean chisloW, chisloG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_first_lesson);
    }

    public void CreateTime(View v)
    {

        timeMondey = (EditText) findViewById(R.id.timeInMonday);
        timeTuesday = (EditText) findViewById(R.id.timeInTuesday);
        timeWednesday = (EditText) findViewById(R.id.timeInWednesday);
        timeThursday = (EditText) findViewById(R.id.timeInThursday);
        timeFriday = (EditText) findViewById(R.id.timeInFriday);
        timeSaturday = (EditText) findViewById(R.id.timeInSaturday);

        int longTextM = timeMondey.getText().toString().length();
        int longTextTues = timeTuesday.getText().toString().length();
        int longTextWen = timeWednesday.getText().toString().length();
        int longTextThur = timeThursday.getText().toString().length();
        int longTextF = timeFriday.getText().toString().length();
        int longTextS = timeSaturday.getText().toString().length();

        if ((chisloW == true) && (chisloG == true))
        {
            if ((longTextM != 0) || (longTextTues != 0) || (longTextWen != 0) || (longTextThur != 0) || (longTextF != 0) || (longTextS != 0))
            {
                if ((longTextM != 0))
                {
                    int time = Integer.parseInt(timeMondey.getText().toString()); //пофиксить часы, нельзя поставить будильник на полночь
                    int dayOfWeek = Calendar.MONDAY;
                    ArrayList<Integer> alarmDaysM = new ArrayList<>();
                    int x = -140;
                    int y = -490;
                    AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval, alarmDaysM);
                }

                if (longTextTues != 0)
                {
                    int time = Integer.parseInt(timeTuesday.getText().toString()); //пофиксить часы, нельзя поставить будильник на полночь
                    int dayOfWeek = Calendar.TUESDAY;
                    ArrayList<Integer> alarmDaysTu = new ArrayList<>();
                    int x = -140;
                    int y = -380;
                    AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval, alarmDaysTu);
                }

                if (longTextWen != 0)
                {
                    int time = Integer.parseInt(timeWednesday.getText().toString()); //пофиксить часы, нельзя поставить будильник на полночь
                    int dayOfWeek = Calendar.WEDNESDAY;
                    ArrayList<Integer> alarmDaysW = new ArrayList<>();
                    int x = -140;
                    int y = -270;
                    AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval, alarmDaysW);
                }

                if (longTextThur != 0)
                {
                    int time = Integer.parseInt(timeTuesday.getText().toString()); //пофиксить часы, нельзя поставить будильник на полночь
                    int dayOfWeek = Calendar.THURSDAY;
                    ArrayList<Integer> alarmDaysTh = new ArrayList<>();
                    int x = -140;
                    int y = -160;
                    AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval, alarmDaysTh);
                }

                if (longTextF != 0)
                {
                    int time = Integer.parseInt(timeFriday.getText().toString()); //пофиксить часы, нельзя поставить будильник на полночь
                    int dayOfWeek = Calendar.FRIDAY;
                    ArrayList<Integer> alarmDaysF = new ArrayList<>();
                    int x = -140;
                    int y = -34;
                    AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval, alarmDaysF);
                }

                if (longTextS != 0)
                {
                    int time = Integer.parseInt(timeSaturday.getText().toString()); //пофиксить часы, нельзя поставить будильник на полночь
                    int dayOfWeek = Calendar.SATURDAY;
                    ArrayList<Integer> alarmDaysS = new ArrayList<>();
                    int x = -140;
                    int y = 80;
                    AlarmCreate(timeGoH, timeGoM, timeWakeUpH, timeWakeUpM, dayOfWeek, time, x, y, howManyWake, howManyOut, timeInterval, alarmDaysS);
                }

                Toast.makeText(this, "Будильики созданы", Toast.LENGTH_LONG).show();
            }
            else
            {
                InputNumber(0,370, "Не введено время ни одной пары");
            }
        }
        else
        {
            InputNumber(0,370, "Не указано доп. время (на подъем и на сборы)");
        }
    }

    public void AlarmCreate(int hTimeG, int mTimeG, int hTimeWake, int mTimeWake, int dayOfWeek, int time, int x, int y, int howWake, int howOut, int interval, ArrayList massivAlarm)
    {
        try
        {
            Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);

            int hTime = time / 100;
            int mTime = time % 100;

            if (hTime < 24 && mTime < 60) //если минуты и часы введены корректно
            {
                hTime -= hTimeG; //вычитаем часы на путь

                if (mTime - mTimeG < 0) //если при вычитании минут на пусть получается минут
                {
                    hTime--; //получается сдвинулся час
                    mTime = 60 + mTime - mTimeG; //и выставляем правильные минуты
                }
                else
                {
                    mTime -= mTimeG;
                }

                int count = 0;

                while (howOut != 0)
                {
                    //тавим будильник на выход
                    intent.putExtra(AlarmClock.EXTRA_HOUR, hTime);
                    intent.putExtra(AlarmClock.EXTRA_MINUTES, mTime);
                    massivAlarm.add(dayOfWeek);
                    intent.putExtra(AlarmClock.EXTRA_DAYS, massivAlarm);
                    startActivity(intent);

                    if (mTime + interval == 60)
                    {
                        hTime++;
                        mTime = 0;
                    }
                    else if (mTime > 60)
                    {
                        mTime -= 60;
                        hTime++;
                    }
                    else
                    {
                        mTime += interval;
                    }

                    count++;
                    howOut--;
                }

                if (count > 0)
                {
                    while (count != 0)
                    {
                        mTime -= interval;
                        count--;
                    }
                }

                hTime -= hTimeWake; //вычитаем время на подъем

                if (mTime - mTimeWake < 0)
                {
                    hTime--;
                    mTime = 60 + mTime - mTimeWake;
                }
                else
                {
                    mTime -= mTimeWake;
                }

                while (howWake != 0)
                {
                    //первый будильник на встать
                    intent.putExtra(AlarmClock.EXTRA_HOUR, hTime);
                    intent.putExtra(AlarmClock.EXTRA_MINUTES, mTime);
                    massivAlarm.add(dayOfWeek);
                    intent.putExtra(AlarmClock.EXTRA_DAYS, massivAlarm);
                    startActivity(intent);

                    if (mTime + interval == 60)
                    {
                        hTime++;
                        mTime = 0;
                    }
                    else if (mTime > 60)
                    {
                        mTime -= 60;
                        hTime++;
                    }
                    else
                    {
                        mTime += interval;
                    }
                    howWake--;
                }
            }
            else
            {
                Toast.makeText(this, "Число не соответствует временным размерностям", Toast.LENGTH_LONG).show();
            }
        }
        catch (NumberFormatException e)
        {
            InputNumber(x,y, "Введите число");
        }
    }

    public void InputNumber(int x, int y, String s)
    {
        Toast toast = Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, x, y);
        toast.show();
    }

    public void delete (View v)
    {
        //AlarmManager.cancel(intent);
    }

    public void onButtonClickSettingOtherTimeWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(TimeFirstLesson.this, SettingOtherTime.class);
        startActivity(intent);
    }

    public void onButtonClickMainActivityWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(TimeFirstLesson.this, MainActivity.class);
        startActivity(intent);
    }

    public void onButtonClickmgtuWindow (View v) //переход на сраницу с инструкций по нажатии кнопки
    {
        Intent intent = new Intent(TimeFirstLesson.this, MgtuBaumanka.class);
        startActivity(intent);
    }
}